// ==UserScript==
// @name		Sound Universe Auto Woot Loader for Tampermonkey (Chrome)
// @namespace	http://www.xuniver.se/
// @version		1.0
// @description	Simple Auto Woot loader.
// @match		https://plug.dj/*
// @copyright	2014, XzaR
// ==/UserScript==


window.addEventListener('DOMContentLoaded',function(e)
{
	setTimeout(function(){
		
		var script = document.createElement('script');
		script.src = 'https://xuniver.se/projects/plug-su-woot/plugin.js';
		document.body.appendChild(script);
		
	},5000);
});