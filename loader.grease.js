// ==UserScript==
// @name        Sound Universe Auto Woot Loader for GreaseMonkey (Firefox)
// @namespace   http://www.xuniver.se/
// @description Simple Auto Woot loader.
// @include     https://plug.dj/*
// @version     1.0
// @grant       none
// ==/UserScript==

window.addEventListener('DOMContentLoaded',function(e)
{
	setTimeout(function(){
		
		var script = document.createElement('script');
		script.src = 'https://xuniver.se/projects/plug-su-woot/plugin.js';
		document.body.appendChild(script);
		
	},5000);
});