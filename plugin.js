function SoundUniverseWoot()
{
	this.version = '1.6';
	this.timerVote;
	this.timerReload;
	
	this.varsKey = 'PlugSuAutoWoot';
	this.vars = {video:1,audio:1,woot:1,avatars:1,refresh:0,meh:0};
	
	this.score = {};
}

SoundUniverseWoot.prototype = {
	init:function(){
	
		var self = this;
	
		var vars = window.localStorage.getItem(this.varsKey);
		if(vars){
		
			this.vars = JSON.parse(vars);
		}
		
		console.log('Sound Universe Auto Woot v' + this.version + '.');
		
		var roomLoaderTries = 0;
		window.setInterval(function(){
			
			if(self.vars.refresh){
			
				var disconnect 	= document.getElementsByClassName('disconnect')[0];
				var dialog 		= document.getElementsByClassName('dialog-frame')[0];
				
				var roomLoader = document.getElementById('room-loader');
				if(roomLoader) roomLoaderTries++;
				else roomLoaderTries = 0;
				
				if(disconnect || dialog || 
					document.title.indexOf("error") != -1 || 
					document.title.indexOf("connecting") != -1 || 
					roomLoaderTries > 2){
					
					window.location.reload();
				}
			}
			
		},1000*30);
	},
	
	load:function()
	{
		this.setAudio();
		this.setVideo();
		this.setAvatars();
		
		var self = this;
		setTimeout(function(){ 
		
			self.score = API.getScore();
			self.setVote();
		
		},4000);
		
		console.log('Sound Universe Auto Woot Loaded.');
	},
	
	save:function()
	{
		window.localStorage.setItem(this.varsKey, JSON.stringify(this.vars));
	},
	
	setVideo:function()
	{
		var playback = document.getElementById('playback');
		if(playback){
		
			if(this.vars.video && playback.innerHTML == '')
				window.location.reload();
			else if(!this.vars.video)
				playback.innerHTML = '';
		}
	},
	
	setAvatars:function(){
	
		var avatars = document.getElementById('audience');
		if(avatars){
		
			if(this.vars.avatars && avatars.innerHTML == '')
				window.location.reload();
			else if(!this.vars.avatars)
				avatars.innerHTML = '';
		}
	},
	
	setAudio:function()
	{
		if(!this.vars.audio)
		{
			API.setVolume(0);
		}
	},
	
	setVote:function()
	{
		if(this.vars.woot){
		
			this.vote();
			if(!this.timerVote){
			
				var self = this;
				this.timerVote = window.setInterval(function(){
				
					self.vote();
				
				},1000*20);
			}
		}
		else
		{
			if(self.timerVote)
				window.clearInterval(self.timerVote);
		}
	},
	
	toggleAudio:function()
	{
		if(this.vars.audio)
			this.vars.audio = 0;
		else
			this.vars.audio = 1;
		
		this.save();
		this.setAudio();
	},
	
	toggleVideo:function()
	{
		if(this.vars.video)
			this.vars.video = 0;
		else
			this.vars.video = 1;
		
		this.save();
		this.setVideo();
	},
	
	toggleAvatars:function()
	{
		if(this.vars.avatars)
			this.vars.avatars = 0;
		else
			this.vars.avatars = 1;
		
		this.save();
		this.setAvatars();
	},
	
	toggleVote:function()
	{
		if(this.vars.woot)
			this.vars.woot = 0;
		else
			this.vars.woot = 1;
		
		this.save();
		this.setVote();
	},
	
	toggleMeh:function()
	{
		if(this.vars.meh)
			this.vars.meh = 0;
		else
			this.vars.meh = 1;
		
		this.save();
	},
	
	toggleRefresh:function()
	{
		if(this.vars.refresh)
			this.vars.refresh = 0;
		else
			this.vars.refresh = 1;
		
		this.save();
	},
	
	vote:function(){
	
		var self = this;
		setTimeout(function(){
		
			if(self.vars.meh == 1 && typeof self.score != 'undefined' && 
				self.score.negative && self.score.negative > 1) self.meh();
			else self.woot();
		
		},1000*(Math.random()*8+5));
	},
	
	woot:function(){
	
		var woot = document.getElementsByClassName('icon-woot')[0];
		if(woot)
			woot.click();
	},
	
	meh:function(){
	
		var meh = document.getElementsByClassName('icon-meh')[0];
		if(meh)
			meh.click();
	},
	
	_userAdvance:function(obj){
		if (obj == null) return;
		
		this.score = {};
		this.setVote();
		
		if(obj.media &&  obj.media.duration > 0){
		
			if(this.timerReload)
				window.clearTimeout(this.timerReload);
				
			this.timerReload = window.setTimeout(function(){window.location.reload();},1000*(obj.media.duration + 30));
		}
	},
	
	_scoreUpdate:function(obj){
		if (obj == null) return;
		this.score = obj;
	},
	
	_voteUpdate:function(obj){
		if (obj == null) return;
		this.score = API.getScore();
	},
	
	showCommands:function(){
	
		API.chatLog("Toggle Commands: /su_audio, /su_video, /su_autowoot, /su_refresh, /su_meh", false);
		
	},
	
	_chatCommand:function(cmd){
		
		if (cmd == null) return;
		
		switch(cmd)
		{
			case '/su_audio':
				this.toggleAudio();
				break;
			case '/su_video':
				this.toggleVideo();
				break;
			case '/su_avatars':
				this.toggleAvatars();
				break;
			case '/su_autowoot':
				this.toggleVote();
				break;
			case '/su_refresh':
				this.toggleRefresh();
				break;
			case '/su_commands':
				this.showCommands();
				break;
			case '/su_meh':
				this.toggleMeh();
				break;
		}
	}
}

var suWoot = new SoundUniverseWoot();
function ___LoadWoot(su)
{
	su.init();
	
	var iTries = 0;
	var su = su;
	
	var findAPI = window.setInterval(function(){
	
		if(typeof API != 'undefined'){
		
			su.load();
			
			API.on(API.ADVANCE, 			function(obj)	{su._userAdvance(obj);});
			API.on(API.CHAT_COMMAND, 		function(data)	{su._chatCommand(data);});
			API.on(API.ROOM_SCORE_UPDATE, 	function(obj)	{su._scoreUpdate(obj);});
			API.on(API.VOTE_UPDATE, 		function(obj)		{su._voteUpdate(obj);});
			window.clearInterval(findAPI);
		}
		else
		{
			if(iTries > 59)
				window.location.reload();
		}
		
		iTries++;
	
	},1000);
}

___LoadWoot(suWoot);